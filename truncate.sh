#!/bin/sh

find /var/log/gitlab -type f | xargs truncate -s 0
find /var/log/gitlab -type f -size 0 | xargs rm
find /var/log/gitlab -type d -size 0 | xargs rmdir
