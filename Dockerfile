FROM alpine
RUN apk add --update rsyslog && rm -rf /var/cache/apk/*
ADD rsyslog.conf /etc/rsyslog.conf
ADD truncate.sh /usr/local/bin/truncate.sh
ADD run-rsyslog.sh /usr/local/bin/run-rsyslog.sh
USER 1000
ENTRYPOINT ["/usr/local/bin/run-rsyslog.sh"]
