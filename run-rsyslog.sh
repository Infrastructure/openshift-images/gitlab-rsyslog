#!/bin/sh

PID_PATH=/var/tmp/rsyslogd.pid

rsyslogd -i ${PID_PATH}

while true; do
  A=$(find /var/log/gitlab -type d | wc -l)
  sleep 3
  B=$(find /var/log/gitlab -type d | wc -l)

  if [ ${B} -gt ${A} ]; then
    echo 'New directory: rolling over'
    pkill -TERM rsyslogd

    if ! [ -f ${PID_PATH} ]; then
      rsyslogd -i ${PID_PATH}
    else
      sleep 3

      rsyslogd -i ${PID_PATH}
    fi
  fi
done
